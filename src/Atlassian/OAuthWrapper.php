<?php
namespace Atlassian;

include('fix/Crypt/RSA.php');

use Crypt_RSA;
use Guzzle\Http\Client;
use Guzzle\Plugin\Oauth\OauthPlugin;

class OAuthWrapper {

	protected $baseUrl;
	protected $sandbox;
	protected $consumerKey;
	protected $consumerSecret;
	protected $callbackUrl;
	protected $requestTokenUrl = 'oauth';
	protected $accessTokenUrl = 'oauth';
	protected $authorizationUrl = 'OAuth.action?oauth_token=%s';

	protected $tokens;

	protected $client;
	protected $oauthPlugin;

	public function __construct($baseUrl) {
		$this->baseUrl = $baseUrl;
	}

	public function requestTempCredentials() {
		return $this->requestCredentials(
			$this->requestTokenUrl . '?oauth_callback=' . $this->callbackUrl	
		);
	}
	
	public function requestAuthCredentials($token, $tokenSecret, $verifier) {
		return $this->requestCredentials(
			$this->accessTokenUrl . '?oauth_callback=' . $this->callbackUrl . '&oauth_verifier=' . $verifier,
			$token,
			$tokenSecret
		);
	}

	protected function requestCredentials($url, $token = false, $tokenSecret = false) {
		$client = $this->getClient($token, $tokenSecret);

		$response = $client->post($url)->send();

		return $this->makeTokens($response);
	}

	protected function makeTokens($response) {
		$body = (string) $response->getBody();

		$tokens = array();
		parse_str($body, $tokens);

		if (empty($tokens)) {
			throw new Exception("An error occurred while requesting oauth token credentials");
		}

		$this->tokens = $tokens;
		return $this->tokens;
	}

	public function getClient($token = false, $tokenSecret = false) {
		if (!is_null($this->client)) {
			return $this->client;
		} else {
			$this->client = new Client($this->baseUrl);

			$privateKey = $this->privateKey;
			$this->oauthPlugin = new OauthPlugin(array(
				'consumer_key' 		=> $this->consumerKey,
				'consumer_secret' 	=> $this->consumerSecret,
				'token' 			=> !$token ? $this->tokens['oauth_token'] : $token,
				'token_secret' 		=> !$token ? $this->tokens['oauth_token_secret'] : $tokenSecret,
	            'signature_method' => 'RSA-SHA1',
	            'signature_callback' => function($stringToSign, $key) use ($privateKey) {
					if (!file_exists($privateKey)) {
						throw new \InvalidArgumentException("Private key {$privateKey} does not exist");
					}
					$key = '-----BEGIN RSA PRIVATE KEY-----MIICWwIBAAKBgQC0Ihjg4kCqbBXIraC8v6aldNp/ECb+TOMidh57Xmw6ZKoHpmaY5I+R546n6JhcmLV+PvonXN6zm6gzZPS+nKCJLGR/zjt63Y/Y8oqNZJk42Oy52hqbnA+7jKPIBq7Edl0JVNgIoTmUJ6zMc9fyhNAiaJh+MKCr8H2UubHVUqtAlwIDAQABAoGAcCk3Anf+ud75JW5vQne3JRkAn9OH5Bot55xS2JBvpWBeusqy4YqXlsru+cALdsSjZcuC6ZWbGYzXi8DV4AYYsqSG42cTeFocAmn0q1/BTmtkFaCwUDzNEB3pD/5buuJbnkNZs6LAJbaB0rLOjI7cWce6uR22s29Jz9nVnVANBhECQQDrQFZaNd/LW1AbQ+E8QsFZGEkv0glQu/TKdN0ozZ19k6CyfDdKgkw4EngO2lU03dntYDfqyS+qHjzPPK4wUqnJAkEAxAVEBXwbtEiLIyCsF5iRoU09wFwtKNWC8vuyChClmbwchm33hBltC6RIyOGJ4YJsfYZ5TFX9AX0UIJnIU2vHXwJACM/9LB3uhzihdJQDYPWDNqoxO6hbjvmrftYH3tps9uJL18PNcm4VGtieRLgrDvLskfbTwWjN70p4rq6wO8Es+QJAeJjEwJsacFAxSetP9TRoZFShKTeMkRLK/bLcVlTnjS2uXwZIwXzjGcZ5AR3BNnyHQrL47X35qnDJcUUreidVAQJAJUQ3kzkZStZ4WEkCgk/ns8Q3pkgKXqI7HE30RZsrI4jI0puTEu2t8zl2Rjfzx2YyzEc40uBSoMgCcQV+oxOY/w==-----END RSA PRIVATE KEY-----';

					$rsa = new Crypt_RSA();
					$rsa->loadKey($key);

					$certificate = openssl_pkey_get_private($rsa);
					echo $certificate;

					$privateKeyId = openssl_get_privatekey($certificate);

					$signature = null;

					openssl_sign($stringToSign, $signature, $privateKeyId);
					openssl_free_key($privateKeyId);

					return $signature;
	            }
			));

			$this->client->addSubscriber($this->oauthPlugin);

			return $this->client;
		}
	}

	public function makeAuthUrl() {
		return $this->baseUrl . sprintf($this->authorizationUrl, urlencode($this->tokens['oauth_token']));
	}

	public function setConsumerKey($consumerKey) {
		$this->consumerKey = $consumerKey;
		return $this;
	}

	public function setConsumerSecret($consumerSecret) {
		$this->consumerSecret = $consumerSecret;
		return $this;
	}

	public function setCallbackUrl($callbackUrl) {
		$this->callbackUrl = $callbackUrl;
		return $this;
	}

	public function setRequestTokenUrl($requestTokenUrl) {
		$this->requestTokenUrl = $requestTokenUrl;
		return $this;
	}

	public function setAccessTokenUrl($accessTokenUrl) {
		$this->accessTokenUrl = $accessTokenUrl;
		return $this;
	}

	public function setAuthorizationUrl($authorizationUrl) {
		$this->authorizationUrl = $authorizationUrl;
		return $this;
	}
	
	public function setPrivateKey($privateKey) {
		$this->privateKey = $privateKey;
		return $this;
	}
}
